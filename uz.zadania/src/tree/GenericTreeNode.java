package tree;

import java.util.ArrayList;
import java.util.List;

public class GenericTreeNode<T> {

    private T data;
    private List<GenericTreeNode<T>> children;
    private GenericTreeNode<T> parent;
    private String name;  //imie jest niekonieczne, dodane dla przejrzystosci przykladu

    public GenericTreeNode(String name) {
        super();
        children = new ArrayList<GenericTreeNode<T>>();
        this.name=name;
    }

    public GenericTreeNode(T data, String name) {
        this(name);
        setData(data);
    }
    
    public String getName() { // notneccesary
    	return this.name;
    }

    public GenericTreeNode<T> getParent() {
        return this.parent;
    }

    public List<GenericTreeNode<T>> getChildren() {
        return this.children;
    }
    
    public int getNumberOfChildren() {
        return getChildren().size();
    }

    public boolean hasChildren() {
        return (getNumberOfChildren() > 0);
    }

    public void setChildren(List<GenericTreeNode<T>> children) {
        for(GenericTreeNode<T> child : children) {
           child.parent = this;
        }

        this.children = children;
    }

    public void addChild(GenericTreeNode<T> child) {
        child.parent = this;
        children.add(child);
    }

    public void addChildAt(int index, GenericTreeNode<T> child) throws IndexOutOfBoundsException {
        child.parent = this;
        children.add(index, child);
    }

    public void removeChildren() {
        this.children = new ArrayList<GenericTreeNode<T>>();
    }

    public void removeChildAt(int index) throws IndexOutOfBoundsException {
        children.remove(index);
    }

    public GenericTreeNode<T> getChildAt(int index) throws IndexOutOfBoundsException {
        return children.get(index);
    }

    public T getData() {
        return this.data;
    }

    //getchildern+
  /*  public  List<GenericTreeNode<T>> getchildrenplus(GenericTreeNode<T> node){
    GenericTreeNode<T> returned;
    	for (GenericTreeNode<T> child:node.children)
    		System.out.println(child.getchildrenplus(child));
    	return child.getchildrenplus(child);
    }*/
    public void setData(T data) {
        this.data = data;
    }

    public String toString() {
        return getData().toString();
    }

    @Override		//zamiast przeciazania operatora == przeciazamy funkcje equals()
    
    public boolean equals(Object obj) {
        if (this == obj) {
           return true;
        }
        if (obj == null) {
           return false;
        }
        if (getClass() != obj.getClass()) {
           return false;
        }
        GenericTreeNode<?> other = (GenericTreeNode<?>) obj; 
        if (data == null) {
           if (other.data != null) {
              return false;
           }
        } else if (!data.equals(other.data)) { 
           return false;
        }
        return true;
    }

}

