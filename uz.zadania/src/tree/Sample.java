package tree;

//import java.awt.List;

public class Sample {

	public static void main(String[] args) {
		GenericTree<String> drzewo = new GenericTree<String>();
		GenericTreeNode<String> lisc1 = new GenericTreeNode<String>("tekst1", "lisc1");
		GenericTreeNode<String> lisc2 = new GenericTreeNode<String>("tekst2", "lisc2");
		GenericTreeNode<String> lisc3 = new GenericTreeNode<String>("tekst2", "lisc2");
		GenericTreeNode<String> lisc4 = new GenericTreeNode<String>("tekst2", "lisc2");
		GenericTreeNode<String> lisc5 = new GenericTreeNode<String>("tekst2", "lisc2");
		GenericTreeNode<String> lisc6 = new GenericTreeNode<String>("tekst2", "lisc2");
		GenericTreeNode<String> korzen = new GenericTreeNode<String>("to jest korzen", "korzen");
		drzewo.setRoot(korzen);
		korzen.addChild(lisc1);
		korzen.addChild(lisc2);
		korzen.addChild(lisc3);
		lisc1.addChild(lisc5);
		lisc5.addChild(lisc6);
		System.out.println(drzewo.getRoot());
		System.out.println("liczba elementow drzewa: " +drzewo.getNumberOfNodes());
		System.out.println("sprawdzenie czy tekst2 istnieje: "+drzewo.exists("tekst2") );
		System.out.println("szukamy stringa tekst1 w drzewie: "+ drzewo.find("tekst1")+ " *** szukany plik znajduje sie w elemencie o nazwie: "+ drzewo.find("tekst1").getName());
		System.out.println("lisc2 parent: " + lisc2.getParent().getName());
		System.out.println("dzieci korzenia:");
		for (GenericTreeNode<String>child:korzen.getChildren()){ 
			System.out.println(child.getChildren());
			}
		
	}

}
