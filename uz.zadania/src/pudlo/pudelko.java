package pudlo;

class Pudelko {
private int m_nZawartosc; //zmienna warunkowa,
private boolean m_bDostepne = false;
public synchronized int wez(Konsument obj)
{   while (m_bDostepne == false)
{  try {   wait(); }
catch (InterruptedException e) { }
}
m_bDostepne = false;
System.out.println("Konsument #" + obj.m_nLiczba //public ?
+ " wyjal: " + this.m_nZawartosc);
notifyAll(); // zawiadomienie Producenta
return m_nZawartosc;
}
public synchronized void wloz(int wartosc, Producent obj)
{   while (m_bDostepne == true)
{   try { wait();   }
catch (InterruptedException e) { }
}
System.out.println("Producent #" + obj.ID
+ " wlozyl: " + wartosc);
m_nZawartosc = wartosc;
m_bDostepne = true;

notifyAll(); //zawiadomienie Konsumenta
}
}